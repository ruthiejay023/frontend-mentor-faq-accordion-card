/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    colors: {
      // primary
      // text
      'very-dark-desaturated-blue': 'hsl(238, 29%, 16%)',
      'soft-red': 'hsl(14, 88%, 65%)',
      
      // background
      'white': 'hsla(0, 0%, 100%, 1)',
      // gradient
      'soft-violet': 'hsl(273, 75%, 66%)',
      'soft-blue': 'hsl(240, 73%, 65%)',

      // neutral
      // text
      'very-dark-grayish-blue': 'hsl(237, 12%, 33%)',
      'dark-grayisy-blue': 'hsl(240, 6%, 50%)',

      // dividers
      'light-grayish-blue': 'hsl(240, 5%, 91%)'
    },
    fontFamily: {
      KumbhSans: ['Kumbh Sans', 'cursive']
    }
  },
  plugins: [],
}

